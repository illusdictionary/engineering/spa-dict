FROM node:16.15.0-alpine3.15
COPY . /var/task/
WORKDIR /var/task/
RUN yarn install
EXPOSE 3000
CMD ["yarn", "start"]
