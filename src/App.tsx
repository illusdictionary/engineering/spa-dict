import './App.css'
import React from 'react'
import { StyledContainer } from './styles'
import { WordList } from './WordList'
import { CategoryList } from './CategoryList'

const App: React.FC = () => {
  return (
    <>
      <StyledContainer>
        <CategoryList/>
        <WordList/>
      </StyledContainer>
    </>
  )
}

export { App }
