import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro'
import { StyledCategories } from './styles'

const CategoryList: React.FC = () => {
  return (
    <StyledCategories>
      <h2>Categories</h2>
      <div>
        <FontAwesomeIcon icon={icon({ name: 'paw', style: 'solid' })}/>
        <button>Animals</button>
      </div>
      <div>
        <FontAwesomeIcon icon={icon({ name: 'car-on', style: 'solid' })}/>
        <button>Cars</button>
      </div>
      <div>
        <FontAwesomeIcon icon={icon({ name: 'utensils', style: 'solid' })}/>
        <button>Cooking</button>
      </div>
      <div>
        <FontAwesomeIcon icon={icon({ name: 'chair', style: 'solid' })}/>
        <button>Furniture</button>
      </div>
      <div>
        <FontAwesomeIcon icon={icon({ name: 'seedling', style: 'solid' })}/>
        <button>Plants</button>
      </div>
    </StyledCategories>
  )
}

export { CategoryList }
