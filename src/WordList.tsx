import React, { useEffect, useState } from 'react'
import axios from 'axios'
import {  StyledWords } from './styles'

interface IWords {
  title: string
  definition: string
  illustration: string
}

const baseClient = axios.create({
  baseURL: process.env.REACT_APP_ENDPOINT as string
})

const WordList: React.FC = () => {
  const [words, setWords] = useState([])
  const [error, setError] = useState()
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    try {
      const fetchWords = async () => {
        const response = await baseClient.get('/words/', {
          headers: {
            Authorization: `Token ${process.env.REACT_APP_TOKEN}`
          }
        })
        setWords(response.data)
      }
      fetchWords()
      setLoading(false)
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (error: any) {
      if (error.response) {
        setError(error)
      } else if (error.request) {
        setError(error)
      } else {
        setError(error)
      }
    }
  }, [])

  if (isLoading) {
    return <div>Loading...</div>
  }
  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  if (error) {
    const { message } = error
    return <div>An error occured: {message}</div>
  }
  return (
    <StyledWords>
      {words.map((word: IWords) => {
        const { title, definition, illustration } = word

        return (
          <article key={title}>
            <div>
              <img
                width='250px'
                height='250px'
                src={illustration}
                alt='word illustration'
              />
            </div>
            <div><h3>{title}</h3></div>
            <div>{word.definition && <p>{definition}</p>}</div>
          </article>
        )
      })}
    </StyledWords>
  )
}

export { WordList }
