import styled from 'styled-components'

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledCategories = styled.div`
  display: flex;
  flex-direction: column;
  width: 296px;
  margin-top: 20px;
  margin-left: 30px;
  margin-right: 60px;
`

const StyledWords = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 50px;
`

export {
  StyledContainer,
  StyledCategories,
  StyledWords
}
